#!/bin/bash

contractContent="$1"
contractTag="$2"
inputFile="$3"

child="SINTESI_GAS"

toZeroIfNotPresent () {
  if [ -z $1 ] ; then
    echo 0
  else
    echo $1
  fi
}


getTagContent () {
  result=$(xmllint --xpath "string($1)" - <<< "$contractContent" | tr -d "." | sed 's/,/\./')
  result=$(toZeroIfNotPresent $result)
  echo "$result"
}


function compareResults () {

  if [ 1 -eq "$(echo "$1 - $2 <= 0.02" | bc)" -a $(echo "$1 - $2 >= -0.02" | bc) -eq 1 ] ; then
    echo "1"
  else
    echo "0"
  fi
}


function getSommaProdottiPeriodi () {

  query="$1"
  xmlElement="$2"

  # echo "$xmlElement"

  content=()
  children=$( xmllint --xpath "count($query/PERIODO_RIFERIMENTO)" - <<< "$xmlElement" )

  if [ ! -z "$xmlElement"  ] && [ ! -z "$children" ]; then

    for i in $(seq 1  $children ) ; do
       xpathQuery="$query/PERIODO_RIFERIMENTO[$i]"
       content+=( $( getSommaProdotti "$xpathQuery" "$xmlElement") )
    done

    sommaTotaleProdottiPeriodi=$( IFS="+"; bc<<<"${content[*]}" )
    echo "$sommaTotaleProdottiPeriodi"

  else
    echo "0"
  fi

}


function getSommaProdottiPeriodi () {

  query="$1"
  xmlElement="$2"

  content=()
  children=$( xmllint --xpath "count($1/PERIODO_RIFERIMENTO)" - <<< "$xmlElement" )

  if [ ! -z "$xmlElement"  ] ; then

    for i in $(seq 1  $children ) ; do
       xpathQuery="$query/PERIODO_RIFERIMENTO[$i]"
       content+=( $( getSommaProdotti "$xpathQuery" "$xmlElement") )

    done

    if [ ! -z $content ] ; then
     sommaTotaleProdottiPeriodi=$( IFS="+"; bc<<<"${content[*]}" )
     echo "$sommaTotaleProdottiPeriodi"
    else
      echo "0"
    fi


  else
    echo "0"
  fi

}



function getSommaProdotti () {

  query="$1"
  xmlElement="$2"

  content=()
  prodottoPath="$query/PRODOTTO"
  children=$( xmllint --xpath "count($prodottoPath)" - <<< "$xmlElement" )

  if [ ! -z "$xmlElement" ] ; then

    for i in $(seq 1 $children); do
      subQuery="$query/PRODOTTO[$i]/PREZZO_TOTALE"
      content+=( $( xmllint --xpath "string($subQuery)" - <<< "$xmlElement" | tr -d "." | sed 's/,/\./' ) )
    done

    totaleProdotti=$( IFS="+"; bc<<<"${content[*]}" )
    echo "$totaleProdotti"

  else
    echo "0"
  fi

}



idContratto=$(getTagContent "$contractTag/CONTRACT_NO")
totaleServiziVendita=$(getTagContent "$contractTag/$child/TOTALE_SERVIZI_DI_VENDITA")
totaleServiziRete=$(getTagContent "$contractTag/$child/TOTALE_SERVIZI_DI_RETE")
totaleImposte=$(getTagContent "$contractTag/$child/TOTALE_IMPOSTE")
iva=$(getTagContent "$contractTag/$child/IVA")
altrePartite=$(getTagContent "$contractTag/$child/TOTALE_ONERI_DIVERSI")
bonusSociale=$(getTagContent "$contractTag/$child/BONUS_SOCIALE")
canoneRAI=$(getTagContent "$contractTag/$child/TOTALE_CANONE_RAI")
totaleDaPagare=$(getTagContent "$contractTag/$child/TOTALE_DA_PAGARE")


# Altre informazioni
consumiFatturati=$(getTagContent "$contractTag/$child/CONSUMI_FATTURATI")
totaleComponentiTariffarie=$(getTagContent "$contractTag/$child/TOTALE_COMPONENTI_TARIFFARIE")
ivaSuOneriDiversi=$(getTagContent "$contractTag/$child/IVA_SU_ONERI_DIVERSI")
oneriDiversi=$(getTagContent "$contractTag/$child/ONERI_DIVERSI")
totaleAltreComponentiConsumer=$(getTagContent "$contractTag/$child/TOTALE_ALTRE_COMPONENTI_CONSUMER")
totaleCTfisseTPTE=$(getTagContent "$contractTag/$child/TOTALE_CT_FISSE_TP_TE")
totaleCorrispettiviUsoRetiServizioMisura=$(getTagContent "$contractTag/$child/TOTALE_CORRISPETTIVI_USO_RETI_SERVIZIO_MISURA")
totaleCostoGenerazioneConCte=$(getTagContent "$contractTag/$child/TOTALE_COSTO_GENERAZIONE_CON_CTE")
totaleFornituraEnergia=$(getTagContent "$contractTag/$child/TOTALE_FORNITURA_ENERGIA")
totaleFornituraEnergiaElettricaImposte=$(getTagContent "$contractTag/$child/TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE")
totaleImponibile=$(getTagContent "$contractTag/$child/TOTALE_IMPONIBILE")
totaleServiziRete_bis=$(getTagContent "$contractTag/$child/TOTALE_SERVIZI_RETE")
totaleTrasportoEnergia=$(getTagContent "$contractTag/$child/TOTALE_TRASPORTO_ENERGIA")
totaleTrasportoPotenza=$(getTagContent "$contractTag/$child/TOTALE_TRASPORTO_POTENZA")
totaleEnergiaPerditeEnergitPerNoi=$(getTagContent "$contractTag/$child/TOTALE_ENERGIA_PERDITE_ENERGIT_PER_NOI")
totaleErt=$(getTagContent "$contractTag/$child/ERT")


# TAG da utilizzare
qfvTag="SEZIONE[@TIPOLOGIA=\"QUOTA_FISSA_VENDITA\"]"
tpvTag="SEZIONE[@TIPOLOGIA=\"TP_VENDITA\"]"
qevTag="SEZIONE[@TIPOLOGIA=\"QUOTA_ENERGIA_VENDITA\"]"
eaTag="SEZIONE[@TIPOLOGIA=\"EA\"]"


# Prefissi /postfissi per le query xpath
# Totali
tagRiepilogo="RIEPILOGO_TOTALE_SEZIONE"
sectQfvRiepilogo="$qfvTag/$tagRiepilogo"
sectTpVenditaRiepilogo="$tpvTag/$tagRiepilogo"
sectQevRiepilogo="$qevTag/$tagRiepilogo"
sectEaRiepilogo="$eaTag/$tagRiepilogo"
# Precedentemente fatturati
tagPrecedenteFatturazione="PRECEDENTEMENTE_FATTURATO"
sectQfvPrec="$qfvTag/$tagPrecedenteFatturazione"
sectTpVenditaPrec="$tpvTag/$tagPrecedenteFatturazione"
sectQevPrec="$qevTag/$tagPrecedenteFatturazione"
sectEaPrec="$eaTag/$tagPrecedenteFatturazione"

# Contenuto complessivo, comprensivo di tag xml, degli elementi xml necessari alle squadrature
qfvContent=$( xmllint --xpath $contractTag/$qfvTag - <<< "$contractContent" )
tpvContent=$( xmllint --xpath $contractTag/$tpvTag - <<< "$contractContent" )
qevContent=$( xmllint --xpath $contractTag/$qevTag - <<< "$contractContent" )
eaContent=$( xmllint --xpath $contractTag/$eaTag - <<< "$contractContent" )

# valori dei campi precedente fatturazione
qfvPrec=$( getTagContent "$contractTag/$sectQfvPrec" )
tpvPrec=$( getTagContent "$contractTag/$sectTpVenditaPrec" )
qevPrec=$( getTagContent "$contractTag/$sectQevPrec" )
eaPrec=$( getTagContent "$contractTag/$sectEaPrec" )

# valori dei campi riepilogo
qfvRiepologo=$( getTagContent "$contractTag/$sectQfvRiepilogo" )
tpVenditaRiepilogo=$( getTagContent "$contractTag/$sectTpVenditaRiepilogo" )
qevRiepilogo=$( getTagContent "$contractTag/$sectQevRiepilogo" )
eaRiepilogo=$( getTagContent "$contractTag/$sectEaRiepilogo")

# TOTALI COMPLESSIVI
qfvTot=$( echo "$qfvRiepologo" | bc )
tpvTot=$( echo "$tpVenditaRiepilogo" | bc )
qevTot=$( echo "$qevRiepilogo" | bc )
eaTot=$( echo "$eaRiepilogo" | bc )


serviziVendita=($qfvTot $tpvTot $qevTot $eaTot)
serviziVenditaTot=$( IFS="+"; bc<<<"${serviziVendita[*]}" )

#TEST 3.1 verifica che la somma dei servizi di vendita + la somma del prezzo energia sia pari al totale dei servizi di vendita presente nella Sintesi
testID="[TEST][3.1]"

echo -n "$testID[$inputFile][$idContratto]"
toBeSummed=($serviziVenditaTot $qeaTot)
currentTot=$( IFS="+"; bc<<<"${toBeSummed[*]}" )

if [[ $(compareResults "$totaleServiziVendita" "$currentTot") -eq 1 ]] ; then
  echo -n "[OK][$totaleServiziVendita][$currentTot]"
else
  echo -n "[FAILED][Totale Servizi di vendita:[$totaleServiziVendita] - Totale Calcolato:[$currentTot] Contenuto array somma toBeSummed:[" $(echo "${toBeSummed[@]}" ) "]]"
fi
echo ""


# TEST 3.2 verifica che la somma dei Prodotti per tutti i periodi della quota fissa vendita sia pari al riepilogo sezione
testID="[TEST][3.2]"

query="$qfvTag"
echo -n "$testID[$inputFile][$idContratto]"
if [ ! -z "$qfvContent" ] ; then
  qfvTotaleCalcolato=$( getSommaProdottiPeriodi "$query" "$qfvContent" )
  qfvTotaleCalcolato=$( echo "$qfvTotaleCalcolato+$qfvPrec"|bc)
  if [[ $(compareResults "$qfvTot" "$qfvTotaleCalcolato") -eq 1 ]] ; then
     echo -n "[OK][$qfvTot][$qfvTotaleCalcolato]"
  else
     echo -n "[FAILED][Totale Quota Fissa Vendita:[$qfvTot] - Totale Calcolato:[$qfvTotaleCalcolato] Distanza:[ $(echo "$qfvTot-$qfvTotaleCalcolato"|bc) ] ]"
  fi
else
  echo -n "[SKIPPING][TAG Quota Fissa Vendita inesistente]"
fi
echo ""


# TEST 3.3 verifica che la somma dei Prodotti per tutti i periodi TP_VENDITA sia pari al riepilogo sezione
testID="[TEST][3.3]"

query="$tpvTag"
echo -n "$testID[$inputFile][$idContratto]"

if [ ! -z "$tpvContent" ] ; then

  tpvTotaleCalcolato=$( getSommaProdottiPeriodi "$query" "$tpvContent" )
  tpvTotaleCalcolato=$( echo "$tpvTotaleCalcolato + $tpvPrec"|bc)

  if [[ $(compareResults "$tpvTot" "$tpvTotaleCalcolato") -eq 1 ]] ; then
     echo -n "[OK][$tpvTot][$tpvTotaleCalcolato]"
  else
     echo -n "[FAILED][Totale TP_VENDITA:[$tpvTot] - Totale Calcolato:[$tpvTotaleCalcolato] Distanza:[ $(echo "$tpvTot-$tpvTotaleCalcolato"|bc) ] ]"
  fi

else
  echo -n "[SKIPPING][TAG TP_VENDITA inesistente]"
fi
echo ""


# TEST 3.4 verifica che la somma dei Prodotti per tutti i periodi della quota energia vendita sia pari al riepilogo sezione
testID="[TEST][3.4]"

query="$qevTag"
echo -n "$testID[$inputFile][$idContratto]"

if [ ! -z "$qevContent" ] ; then

  query="$query"
  xmlElement="$qevContent"

  qevTotaleCalcolato=$( getSommaProdottiPeriodi "$query" "$qevContent")
  qevTotaleCalcolato=$( echo "$qevTotaleCalcolato+$qevPrec"|bc)

  if [[ $(compareResults "$qevTot" "$qevTotaleCalcolato") -eq 1 ]] ; then
     echo -n "[OK][$qevTot][$qevTotaleCalcolato]"
  else
     echo -n "[FAILED][Totale Quota Energia Vendita:[$qevTot] - Totale Calcolato:[$qevTotaleCalcolato] Distanza:[ $(echo "$qevTot-$qevTotaleCalcolato"|bc) ] ]"
  fi

 else
  echo -n "[SKIPPING][TAG Quota Energia Vendita inesistente]"
fi
echo ""


#TEST 3.5 verifica che la somma dei Prodotti per tutti i periodi del prezzo energia sia pari al riepilogo sezione
testID="[TEST][3.5]"

query="$eaTag"
echo -n "$testID[$inputFile][$idContratto]"

if [ ! -z "$eaContent" ] ; then
  eaTotaleCalcolato=$( getSommaProdottiPeriodi "$query" "$eaContent")
  eaTotaleCalcolato=$( echo "$eaTotaleCalcolato+$eaPrec"|bc)
  if [[ $(compareResults "$eaTot" "$eaTotaleCalcolato") -eq 1 ]] ; then
     echo -n "[OK][$eaTot][$eaTotaleCalcolato]"
  else
     echo -n "[FAILED][Totale Prezzo Energia (EA) :[$eaTot] - Totale Calcolato:[$eaTotaleCalcolato] Distanza:[ $(echo "$eaTot-$eaTotaleCalcolato"|bc) ] ]"
  fi
else
  echo -n "[SKIPPING][TAG Prezzo Energia (EA) inesistente]"
fi
echo ""

echo ""
