#!/bin/bash

# parameters: <filePath> <output flag> <root> <node> <attributeName> <attributeValue>]
# usage1: nodefind.sh <filePath> <output flag> <root> <node>
# usage2: nodefind.sh <filePath> <output flag> <root> <node> <attributeName> <attributeValue>
# output: a string containing an xml (output flag=xml) or a string contaning indented xml content (output flag=string)

# example1: ./nodefind.sh ./000329-2018.xml xml LOTTO_FATTURE/DOCUMENTO/CONTRATTO_ENERGIA SINTESI_ENERGIA
# example2: ./nodefind.sh ./000329-2018.xml string LOTTO_FATTURE/DOCUMENTO SEZIONE TIPOLOGIA ONERI_DIVERSI
# xpath syntax can be used too (either in root parameter or in node parameter):
# example3: ./nodefind.sh ./000329-2018.xml string LOTTO_FATTURE/DOCUMENTO SEZIONE[@TIPOLOGIA=\"ONERI_DIVERSI\"]

filePath="$1" # an xml file
flag="$2"     # "text" "xml"
root="$3"     # prefix of the node to be found
node="$4"     # name of the node to be found
attributeName="$5" # node's attribute to be found
attributeValue="$6" # attribute's value to be found

if [ "$#" -eq 6 ]; then
    sections=$(echo cat //$root/$node[@$attributeName=\"$attributeValue\"] | xmllint --shell "$filePath"| awk 'NR>2 {print last} {last=$0}'|sed '/-------/d')

  if [ "$flag" == "xml" ]; then
      echo "$sections"
  elif [ "$flag" == "string" ]; then
    sections=$(xmllint --loaddtd $filePath --xpath "string(//$node[@$attributeName=\"$attributeValue\"])" <<< "$sections")
    echo "$sections"
  else
    echo "Wrong flag parameter. Allowed values: xml,string"
  fi

elif [ "$#" -eq 4 ]; then

  sections=$(echo cat //$root/$node | xmllint --shell "$filePath"| awk 'NR>2 {print last} {last=$0}'|sed '/-------/d')

  if [ "$flag" == "xml" ]; then
    echo "$sections"
  elif [[ "$flag" == "string" ]]; then
    sections=$(xmllint --loaddtd $filePath --xpath "string(//$node)" <<< "$sections")
    echo "$sections"
  else
    echo "Wrong flag parameter. Allowed values: xml,string"
  fi

else
  echo "Wrong number of parameters: at least 4 parameters needed."
fi
