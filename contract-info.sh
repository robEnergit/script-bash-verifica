#!/bin/bash

file=$1

out1=$( echo $(nodefind.sh "$file" xml LOTTO_FATTURE/DOCUMENTO CONTRATTO_ENERGIA | grep "CONTRATTO_ENERGIA" | wc -l)"/2" | bc)
out2=$(bc<<< $( echo $(nodefind.sh "$file" xml LOTTO_FATTURE/DOCUMENTO CONTRATTO_DEPOSITO_CAUZIONALE | grep "CONTRATTO_DEPOSITO_CAUZIONALE" | wc -l) "/2"))
out3=$(bc<<< $( echo $(nodefind.sh "$file" xml LOTTO_FATTURE/DOCUMENTO CONTRATTO_GAS | grep "CONTRATTO_GAS" | wc -l) "/2"))

tot=$((out1+out2+out3))

contrattiEnergia=()
for i in $( seq 1 $out1 ) ; do
  currentContract=$( xmllint --xpath "//LOTTO_FATTURE/DOCUMENTO/CONTRATTO_ENERGIA[$i]/CONTRACT_NO/text()" "$file" )
  contrattiEnergia+=($currentContract",")
done

contrattiDepositoCauzionale=()
for i in $( seq 1 $out2 ) ; do
  currentContract=$( xmllint --xpath "//LOTTO_FATTURE/DOCUMENTO/CONTRATTO_DEPOSITO_CAUZIONALE[$i]/CONTRACT_NO/text()" "$file" )
  contrattiDepositoCauzionale+=($currentContract",")
done

for i in $( seq 1 $out3 ) ; do
  currentContract=$( xmllint --xpath "//LOTTO_FATTURE/DOCUMENTO/CONTRATTO_GAS[$i]/CONTRACT_NO/text()" "$file" )
  contrattiGas+=($currentContract",")
done

echo
echo "                      Codice Cliente: $(xmllint --xpath "//LOTTO_FATTURE/DOCUMENTO/CODICE_CLIENTE/text()" "$file")"
echo "                             Invoice: $(xmllint --xpath "//LOTTO_FATTURE/DOCUMENTO/INVOICE_NO/text()" "$file")"
echo "     Numero Documento (Nome Fattura): $(xmllint --xpath "//LOTTO_FATTURE/DOCUMENTO/NUMERO_DOCUMENTO/text()" "$file")"
echo "           Totale Contratti presenti: $tot"
echo "                   Contratti Energia: $out1"
echo "                       Contratti Gas: $out3"
echo "       Contratti Deposito Cauzionale: $out2"
echo
echo "            Elenco contratti energia: $(echo ${contrattiEnergia[@]})"
echo
echo "            Elenco contratti energia: $(echo ${contrattiGas[@]})"
echo
echo "Elenco contratti Deposito Cauzionale: $(echo ${contrattiDepositoCauzionale[@]})"
echo
