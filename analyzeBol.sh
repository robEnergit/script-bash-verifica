#!/bin/bash

sourceDir="$1"

bolList=$(ls -l "$sourceDir"*.bol | awk '{print $NF}' )

for f in $bolList ; do
  line=$(tail -n 1 $f)
  pdf=$(echo "$line" |cut -c255-271 )
  lastLine=$(echo "$line" |cut -c275-302 )
  fattureProdotte=$( bc <<< -1+$(cat $f | wc -l) )
  echo "$fattureProdotte $pdf ${lastLine: -12}"
done
