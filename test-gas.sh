#!/bin/bash
inputFile="$1"

possiedeContrattoDepositoCauzionale ()
{
  out=$(bc<<< $( echo $(./nodefind.sh "$1" xml LOTTO_FATTURE/DOCUMENTO CONTRATTO_DEPOSITO_CAUZIONALE | grep "CONTRATTO_DEPOSITO_CAUZIONALE" | wc -l) "/2"))
  if [[ "$out" == "warning:*" ]] ; then
    echo ""
    echo "ERROR: $out"
    echo ""
  else
   result=$((out))
   echo "$result"
  fi
}


if [ -z $2 ] ; then
  numContracts=$( ./contract-counter.sh "$inputFile" )
else
  numContracts="$2"
fi

#fileContent=$(cat $inputFile)

contractRoot="LOTTO_FATTURE/DOCUMENTO"
contractTag="CONTRATTO_GAS"

# if [ -z $numContracts ] ; then


if [ "$(possiedeContrattoDepositoCauzionale $inputFile)" -le 0 ] ; then
  echo "[PRE][TEST][0.1][$inputFile][Contenuto: $numContracts contratti]"

    for i in $( seq 1 $numContracts ) ; do

        #currentContract=$( xmllint --xpath $contractRoot/$contractTag[$i] $inputFile )

        result=$( gas-test-sr.sh "$i" "$contractTag" "$inputFile" )
        echo "$result"
        #result=$( gas-test-sv.sh "$currentContract" "$contractTag" "$inputFile" )
        #echo "$result"

    done

else
  # Se il contratto è di deposito cauzionale, allora non effettuo i test standard:  produrrebbero solo linee d'errore.
  result="[PRE][TEST][0.2][$inputFile][Contenuto: $numContracts CONTRATTO DEPOSITO CAUZIONALE]"
  echo "$result"
fi

# else
#   echo "[PRE][TEST][0.1][FAILED][$inputFile][NON CONTIENE CONTRATTI ($numContracts) contratti]"
# fi
