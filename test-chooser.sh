#!/bin/bash
inputFile="$1"
outFile="$2"

if [ -z $inputFile ] ; then
  echo "[FAILED][File [$inputFile] inesistente]"
else

  contratti="$( contract-counter.sh "$inputFile" )"
  if [[ ("$contratti" == 0) ]] ; then
    echo "[FAILED][$inputFile][Nessun contratto presente: $contratti]" >> "$outFile"
  elif [[ ("$contratti" == 1) ]] ; then
    echo "[MONO][$inputFile]" >> "$outFile"
    energia=$(conta-energia.sh $inputFile)
    gas=$(conta-gas.sh $inputFile)
    if (( energia > 0 )) ; then
      echo "[ENERGIA][$inputFile]" >> "$outFile"
      test-energia.sh "$inputFile" "$contratti" >> "$outFile"
    fi
    if (( gas > 0 )) ; then
      echo "[GAS][$inputFile]" >> "$outFile"
      test-gas.sh "$inputFile" >> "$outFile"
    fi
  else
    echo "[MULTI][$inputFile]" >> "$outFile"
    test-energia.sh "$inputFile" "$contratti" >> "$outFile"
  fi
  echo "" >> "$outFile"

fi
