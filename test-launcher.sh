#!/bin/bash

# input:
#      $1 directory sulla quale eseguire il test
#      $2 file di output

dir="$1"
out="$2"

echo "" > "$out"

files=$(ls -plhaF $dir*.xml |awk '{print $NF}')
files=($files)

START=$(date +%s.%N)
echo "TIME START: " $(date '+%d/%m/%Y %H:%M:%S') >> "$out"
length="${#files[@]}"
let "i=0"
for current in ${files[@]}; do
  let "i=i+1"
  echo "$current $i/$length"
  test-chooser.sh "$current" "$out" >> "$out"

done

END=$(date +%s.%N)
echo "TIME END: " $(date '+%d/%m/%Y %H:%M:%S') >> "$out"
DIFF=$(echo "$END - $START" | bc)
echo "Ellapsed time: $DIFF seconds" >> "$out"
echo ""


echo ""
echo ""
output=$( cat "$out" | grep "\[FAILED\]" )

#cleaned=$( for i in "${output//][/$'\n'}"; do echo "$i"; done )

cleaned=$( echo "$output" | tr -s '[]' ' '| awk '{print $3, $1,$2 }' )

echo -n "Found "$( echo "$cleaned" | wc -l )" Errors "
echo -n "in "$(ls "$dir"*.xml | wc -l)" files."
echo "Files with errors:"
echo "$cleaned"
