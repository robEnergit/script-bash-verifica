#!/bin/bash
filePath="$1"

out1=$( echo $(nodefind.sh "$filePath" xml LOTTO_FATTURE/DOCUMENTO CONTRATTO_GAS | grep "CONTRATTO_GAS" | wc -l)"/2" | bc)
if [[ "$out1" == "warning:*" ]] ; then
  echo ""
  echo "ERROR: $out1"
  echo ""
else
 result=$((out1))
 echo "$result"
#echo "$out1"
fi
