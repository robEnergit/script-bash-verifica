#!/bin/bash

filePath="$1"
out1=$( echo $(nodefind.sh "$filePath" xml LOTTO_FATTURE/DOCUMENTO CONTRATTO_ENERGIA | grep "CONTRATTO_ENERGIA" | wc -l)"/2" | bc)
out2=$(bc<<< $( echo $(nodefind.sh "$filePath" xml LOTTO_FATTURE/DOCUMENTO CONTRATTO_DEPOSITO_CAUZIONALE | grep "CONTRATTO_DEPOSITO_CAUZIONALE" | wc -l) "/2"))
out3=$(bc<<< $( echo $(nodefind.sh "$filePath" xml LOTTO_FATTURE/DOCUMENTO CONTRATTO_GAS | grep "CONTRATTO_GAS" | wc -l) "/2"))

if [[ "$out1" == "warning:*" ]] ; then
  echo ""
  echo "ERROR: $out1"
  echo ""
else
 result=$((out1+out2+out3))
 echo "$result"
#echo "$out1"
fi
