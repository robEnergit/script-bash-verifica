#!/bin/bash
contractRoot="LOTTO_FATTURE/DOCUMENTO"


contract_pos="$1"
contractTag="$2"
inputFile="$3"


contractContent=$(xmllint --xpath $contractRoot/$contractTag[$contract_pos] $inputFile)

child="SINTESI_GAS"



toZeroIfNotPresent () {
  if [ -z $1 ] ; then
    echo 0
  else
    echo $1
  fi
}


getTagContent () {
  result=$(xmllint --xpath "string($1)" - <<< "$contractContent" | tr -d "." | sed 's/,/\./')
  result=$(toZeroIfNotPresent $result)
  echo "$result"
}


function compareResults()
{
  if [ 1 -eq "$(echo "$1 - $2 <= 0.02" | bc)" -a $(echo "$1 - $2 >= -0.02" | bc) -eq 1 ] ; then
    echo "1"
  else
    echo "0"
  fi

}

idContratto=$(getTagContent "$contractTag/CONTRACT_NO")
totaleServiziVendita=$(getTagContent "$contractTag/$child/TOTALE_SERVIZI_DI_VENDITA")
totaleServiziRete=$(getTagContent "$contractTag/$child/TOTALE_SERVIZI_DI_RETE")
totaleImposte=$(getTagContent "$contractTag/$child/TOTALE_IMPOSTE")
iva=$(getTagContent "$contractTag/$child/IVA")
altrePartite=$(getTagContent "$contractTag/$child/TOTALE_ONERI_DIVERSI")
bonusSociale=$(getTagContent "$contractTag/$child/BONUS_SOCIALE")
canoneRAI=$(getTagContent "$contractTag/$child/TOTALE_CANONE_RAI")
totaleDaPagare=$(getTagContent "$contractTag/$child/TOTALE_DA_PAGARE")

# Altre informazioni
consumiFatturati=$(getTagContent "$contractTag/$child/CONSUMI_FATTURATI")
totaleComponentiTariffarie=$(getTagContent "$contractTag/$child/TOTALE_COMPONENTI_TARIFFARIE")
ivaSuOneriDiversi=$(getTagContent "$contractTag/$child/IVA_SU_ONERI_DIVERSI")
oneriDiversi=$(getTagContent "$contractTag/$child/ONERI_DIVERSI")
totaleAltreComponentiConsumer=$(getTagContent "$contractTag/$child/TOTALE_ALTRE_COMPONENTI_CONSUMER")
totaleCTfisseTPTE=$(getTagContent "$contractTag/$child/TOTALE_CT_FISSE_TP_TE")
totaleCorrispettiviUsoRetiServizioMisura=$(getTagContent "$contractTag/$child/TOTALE_CORRISPETTIVI_USO_RETI_SERVIZIO_MISURA")
totaleCostoGenerazioneConCte=$(getTagContent "$contractTag/$child/TOTALE_COSTO_GENERAZIONE_CON_CTE")
totaleFornituraEnergia=$(getTagContent "$contractTag/$child/TOTALE_FORNITURA_ENERGIA")
totaleFornituraEnergiaElettricaImposte=$(getTagContent "$contractTag/$child/TOTALE_FORNITURA_ENERGIA_ELETTRICA_E_IMPOSTE")
totaleImponibile=$(getTagContent "$contractTag/$child/TOTALE_IMPONIBILE")
totaleServiziRete_bis=$(getTagContent "$contractTag/$child/TOTALE_SERVIZI_RETE")
totaleTrasportoEnergia=$(getTagContent "$contractTag/$child/TOTALE_TRASPORTO_ENERGIA")
totaleTrasportoPotenza=$(getTagContent "$contractTag/$child/TOTALE_TRASPORTO_POTENZA")
totaleEnergiaPerditeEnergitPerNoi=$(getTagContent "$contractTag/$child/TOTALE_ENERGIA_PERDITE_ENERGIT_PER_NOI")
totaleErt=$(getTagContent "$contractTag/$child/ERT")


testID="[TEST][1.1]"
test1result=$(IFS="+";bc<<<"${toBeSummed[*]}")

# Verifica che la somma dei totali parziali individuati in $node corrisponda al totale da pagare riportato
echo -n "$testID[$inputFile][$idContratto]"
toBeSummed=("$totaleServiziVendita" "$totaleServiziRete" "$totaleImposte" "$iva" "$altrePartite" "$bonusSociale" "$canoneRAI")
test1result=$(IFS="+";bc<<<"${toBeSummed[*]}")

#testLog  "$totaleDaPagare" "$test1test1Result"

if [[ $(compareResults "$totaleDaPagare" "$test1result") -eq 1 ]] ; then
  echo -n "[OK]"
else
  echo -n "[FAILED]"
  echo -n "[Contenuto array somma toBeSummed:[" $(echo ${toBeSummed[@]} ) "] Risultato Test:[$test1result] Valore Atteso:[$totaleDaPagare]] Scostamento:[$(bc <<< $test1result-$totaleDaPagare)]"
fi
echo ""



#################################



#"[TEST 2] Verifica della corrispondenza tra gli elementi di \"Energia Elettrica Sintesi\" (totali) rispetto i valori riportati nei SERVIZI DI RETE"

# Recupero le informazioni riguardanti la porzione SERVIZI DI RETE


# Porzioni di XML coinvolte dal confronto
sectQer="SEZIONE[@TIPOLOGIA=\"QUOTA_ENERGIA_RETI\"]"
sectTp="SEZIONE[@TIPOLOGIA=\"TP\"]"
sectCtf="SEZIONE[@TIPOLOGIA=\"CT_FISSE\"]"
sectErt="SEZIONE[@TIPOLOGIA=\"ERT\"]"

qer=$( getTagContent "$contractTag/$sectQer" )
tp=$( getTagContent "$contractTag/$sectTp" )
ctf=$( getTagContent "$contractTag/$sectCtf" )
ert=$( getTagContent "$contractTag/$sectErt" )


toBeSummed=($qer $tp $ctf $ert)
currentTot=$(IFS="+";bc<<<"${toBeSummed[*]}")


#Verifica che il totale \"TOTALE SERVIZI RETE\" di \"Energia Elettrica Sintesi\" coincida con il totale riportato in SINTESI_ENERGIA (TOTALE_SERVIZI_RETE)
testID="[TEST][2.1]"

echo -n "$testID[$inputFile][$idContratto]"

if [[ $(compareResults "$totaleServiziRete_bis" "$currentTot") -eq 1 ]] ; then
  echo -n "[OK]"
else
  echo -n "[FAILED][Totale Servizi di rete:[$totaleServiziRete_bis] - Totale Calcolato:[$currentTot] Contenuto array somma toBeSummed:[" $(echo ${toBeSummed[@]} ) "] - Risultato totale calcolato:[$currentTot] Valore Atteso:[$totaleServiziRete_bis]]"
fi
echo ""

#Verifica che il sub totale di \"Quota Fissa\"  in \"Energia Elettrica Sintesi\" coincida con le cifre riportate in SINTESI_ENERGIA (TOTALE_COMPONENTI_TARIFFARIE)
testID="[TEST][2.2]"
echo -n "$testID[$inputFile][$idContratto]"

if [[ $(compareResults "$totaleComponentiTariffarie" "$ctf") -eq 1 ]] ; then
 echo -n "[OK]"
else
 echo -n "[FAILED][Totale Componenti Tariffarie:[$totaleComponentiTariffarie] - Totale Calcolato:[$ctf]]"
fi
echo ""


#Verifica che il sub totale di \"Quota Potenza\"  in \"Energia Elettrica Sintesi\" coincida con le cifre riportate in SINTESI_ENERGIA (TOTALE_TRASPORTO_POTENZA)
testID="[TEST][2.3]"
echo -n "$testID[$inputFile][$idContratto]"

if [[ $(compareResults "$totaleTrasportoPotenza" "$tp") -eq 1 ]] ; then
 echo -n "[OK]"
else
 echo -n "[FAILED][Totale Trasporto Potenza:[$totaleTrasportoPotenza] - Totale Calcolato:[$tp]]"
fi
echo ""

# Verifica che il sub totale di \"Quota Energia\"  in \"Energia Elettrica Sintesi\" coincida con le cifre riportate in SINTESI_ENERGIA (TOTALE_ALTRE_COMPONENTI_CONSUMER)
testID="[TEST][2.4]"
echo -n "$testID[$inputFile][$idContratto]"

realTot=$( echo "$qer+$ert" | bc )

if [[ $(compareResults "$totaleAltreComponentiConsumer" "$realTot")  -eq 1 ]] ; then
 echo -n "[OK]"
else
 echo -n "[FAILED][Totale Componenti Tariffarie:[$totaleAltreComponentiConsumer] - Totale Calcolato:[$realTot]]"
fi
echo ""
